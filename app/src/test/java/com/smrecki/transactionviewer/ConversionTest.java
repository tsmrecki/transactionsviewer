package com.smrecki.transactionviewer;

import com.smrecki.transactionviewer.helpers.TransactionConverter;
import com.smrecki.transactionviewer.model.ConversionRate;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ConversionTest {
    @Test
    public void conversionRateTest() throws Exception {
        List<ConversionRate> conversionRateList = new ArrayList<>();
        conversionRateList.add(new ConversionRate("USD", "GBP", 0.77f));
        conversionRateList.add(new ConversionRate("CAD", "USD", 0.92f));

        assertEquals(0.77f, TransactionConverter.getConversionRate("USD", "GBP", conversionRateList), 0.01f);
    }

    @Test
    public void finalConversionRateTest() throws Exception {
        List<ConversionRate> conversionRateList = new ArrayList<>();
        conversionRateList.add(new ConversionRate("USD", "GBP", 0.77f));
        conversionRateList.add(new ConversionRate("CAD", "USD", 0.92f));

        assertEquals(0.77f * 0.92f, TransactionConverter.getFinalConversionRate("CAD", "GBP", conversionRateList), 0.01f);
    }

}