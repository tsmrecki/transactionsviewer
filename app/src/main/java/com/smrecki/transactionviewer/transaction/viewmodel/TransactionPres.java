package com.smrecki.transactionviewer.transaction.viewmodel;

/**
 * Created by tomislav on 27/08/16.
 */
public class TransactionPres {

    private float amountCurrency;
    private String currency;
    private float amountGBP;

    public TransactionPres(float amountCurrency, String currency, float amountGBP) {
        this.amountCurrency = amountCurrency;
        this.currency = currency;
        this.amountGBP = amountGBP;
    }

    public float getAmountCurrency() {
        return amountCurrency;
    }

    public String getCurrency() {
        return currency;
    }

    public float getAmountGBP() {
        return amountGBP;
    }
}
