package com.smrecki.transactionviewer.transaction;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smrecki.transactionviewer.R;
import com.smrecki.transactionviewer.transaction.viewmodel.TransactionPres;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by tomislav on 27/08/16.
 */
public class TransactionsAdapter extends RecyclerView.Adapter<TransactionsAdapter.TransactionViewHolder> {

    private List<TransactionPres> transactionPresList = new ArrayList<>();


    public void setTransactionPresList(List<TransactionPres> transactionPresList) {
        this.transactionPresList.clear();
        this.transactionPresList.addAll(transactionPresList);
        notifyDataSetChanged();
    }

    @Override
    public TransactionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TransactionViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.transaction_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(TransactionViewHolder holder, int position) {
        TransactionPres transaction = transactionPresList.get(position);
        holder.originalCurrencyAmount.setText(transaction.getAmountCurrency() + " " + transaction.getCurrency());
        holder.finalCurrencyAmount.setText(transaction.getAmountGBP() + " GBP");
    }

    @Override
    public int getItemCount() {
        return transactionPresList.size();
    }

    protected class TransactionViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.original_currency_amount)
        TextView originalCurrencyAmount;
        @BindView(R.id.final_currency_amount)
        TextView finalCurrencyAmount;

        public TransactionViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}
