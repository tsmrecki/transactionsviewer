package com.smrecki.transactionviewer.transaction;

import com.smrecki.transactionviewer.presentation.presenters.BasePresenter;
import com.smrecki.transactionviewer.presentation.ui.BaseView;
import com.smrecki.transactionviewer.transaction.viewmodel.TransactionPres;

import java.util.List;

/**
 * Created by tomislav on 27/08/16.
 */
public interface TransactionsPresenter extends BasePresenter {

    interface View extends BaseView {
        void showTransactions(List<TransactionPres> transactionPresList);
    }

}
