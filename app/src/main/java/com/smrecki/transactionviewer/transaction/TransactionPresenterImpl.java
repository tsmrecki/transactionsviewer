package com.smrecki.transactionviewer.transaction;

import com.smrecki.transactionviewer.data.DataRepository;
import com.smrecki.transactionviewer.helpers.TransactionConverter;
import com.smrecki.transactionviewer.model.ConversionRate;
import com.smrecki.transactionviewer.model.Transaction;
import com.smrecki.transactionviewer.presentation.ui.BaseView;
import com.smrecki.transactionviewer.transaction.viewmodel.TransactionPres;

import java.lang.ref.WeakReference;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func2;
import rx.schedulers.Schedulers;

/**
 * Created by tomislav on 27/08/16.
 */
public class TransactionPresenterImpl implements TransactionsPresenter {

    private WeakReference<View> view;

    private List<TransactionPres> transactions;
    private DataRepository dataRepository;
    private String productName;
    private String toCurrency;

    public TransactionPresenterImpl(DataRepository dataRepository, String productName, String toCurrency) {
        this.dataRepository = dataRepository;
        this.productName = productName;
        this.toCurrency = toCurrency;
    }

    @Override
    public void bindView(BaseView view) {
        this.view = new WeakReference<>((View) view);

        if (transactions == null) {
            loadTransactions();
        } else {
            this.view.get().showTransactions(transactions);
        }
    }


    void loadTransactions() {

        Observable.zip(dataRepository.getTransactions(productName), dataRepository.getRates(), new Func2<List<Transaction>, List<ConversionRate>, List<TransactionPres>>() {
            @Override
            public List<TransactionPres> call(List<Transaction> transactions, List<ConversionRate> conversionRates) {
                return TransactionConverter.convertTransactions(transactions, conversionRates, toCurrency);
            }
        }).subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<TransactionPres>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(List<TransactionPres> transactionPres) {
                        transactions = transactionPres;
                        if (view != null) {
                            view.get().showTransactions(transactionPres);
                        }
                    }
                });

    }


    @Override
    public void resume() {

    }

    @Override
    public void pause() {
        this.view = null;
    }

    @Override
    public void destroy() {

    }

}
