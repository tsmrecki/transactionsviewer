package com.smrecki.transactionviewer.transaction;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.smrecki.transactionviewer.Dataset;
import com.smrecki.transactionviewer.R;
import com.smrecki.transactionviewer.data.JsonFileRepositoryImpl;
import com.smrecki.transactionviewer.presentation.ui.BaseActivity;
import com.smrecki.transactionviewer.transaction.viewmodel.TransactionPres;

import java.util.List;

import butterknife.BindView;

public class TransactionsActivity extends BaseActivity<TransactionsPresenter> implements TransactionsPresenter.View {

    private static final String PRODUCT_NAME = "product_name";
    @BindView(R.id.transactions_rv)
    RecyclerView transactionsRV;
    TransactionsAdapter transactionsAdapter;
    private String productName;

    public static Intent buildIntent(String productName, Activity callingActivity) {
        Intent i = new Intent(callingActivity, TransactionsActivity.class);
        i.putExtra(PRODUCT_NAME, productName);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (getIntent() != null) {
            productName = getIntent().getStringExtra(PRODUCT_NAME);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.transaction_for, productName));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        transactionsRV.setLayoutManager(new LinearLayoutManager(this));
        transactionsRV.setItemAnimator(new DefaultItemAnimator());

        transactionsAdapter = new TransactionsAdapter();
        transactionsRV.setAdapter(transactionsAdapter);

    }

    @Override
    public TransactionsPresenter initPresenter() {
        return new TransactionPresenterImpl(JsonFileRepositoryImpl.getInstance(this, Dataset.DATASET_NAME), productName, "GBP");
    }


    @Override
    public void showTransactions(List<TransactionPres> transactionPresList) {
        transactionsAdapter.setTransactionPresList(transactionPresList);
    }
}
