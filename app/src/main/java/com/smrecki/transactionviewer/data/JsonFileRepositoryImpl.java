package com.smrecki.transactionviewer.data;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.smrecki.transactionviewer.model.ConversionRate;
import com.smrecki.transactionviewer.model.Product;
import com.smrecki.transactionviewer.model.Transaction;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by tomislav on 27/08/16.
 */
public class JsonFileRepositoryImpl implements DataRepository {

    private static JsonFileRepositoryImpl instance;

    private Context context;
    private List<Transaction> transactionList;
    private List<ConversionRate> conversionRates;
    private String datasetName;

    private JsonFileRepositoryImpl(Context context, String datasetName) {
        this.context = context;
        this.datasetName = datasetName;
    }

    public static JsonFileRepositoryImpl getInstance(Context context, String datasetName) {
        if (instance == null) instance = new JsonFileRepositoryImpl(context, datasetName);
        return instance;
    }

    @Override
    public Observable<List<ConversionRate>> getRates() {
        return Observable.create(new Observable.OnSubscribe<List<ConversionRate>>() {
            @Override
            public void call(Subscriber<? super List<ConversionRate>> subscriber) {
                if (conversionRates == null) {
                    try {
                        InputStream is = openInputStream(datasetName + "/rates.json");
                        Type listType = new TypeToken<ArrayList<ConversionRate>>() {
                        }.getType();
                        conversionRates = loadObject(is, listType);
                        is.close();

                    } catch (IOException e) {
                        subscriber.onError(e);
                    }
                }
                subscriber.onNext(conversionRates);
                subscriber.onCompleted();

            }
        });
    }

    @Override
    public Observable<List<Transaction>> getTransactions() {
        return Observable.create(new Observable.OnSubscribe<List<Transaction>>() {
            @Override
            public void call(Subscriber<? super List<Transaction>> subscriber) {
                if (transactionList == null) {
                    try {
                        transactionList = loadTransactions(datasetName);
                    } catch (IOException e) {
                        e.printStackTrace();
                        subscriber.onError(e);
                        return;
                    }
                }

                subscriber.onNext(transactionList);
                subscriber.onCompleted();
            }
        });
    }

    @Override
    public Observable<List<Transaction>> getTransactions(final String productName) {
        return Observable.create(new Observable.OnSubscribe<List<Transaction>>() {
            @Override
            public void call(Subscriber<? super List<Transaction>> subscriber) {
                if (transactionList == null) {
                    try {
                        transactionList = loadTransactions(datasetName);
                    } catch (IOException e) {
                        e.printStackTrace();
                        subscriber.onError(e);
                        return;
                    }
                }

                List<Transaction> productTransactions = new ArrayList<>();
                for (Transaction t : transactionList) {
                    if (t.getProductName().equals(productName))
                        productTransactions.add(t);
                }

                subscriber.onNext(productTransactions);
                subscriber.onCompleted();
            }
        });
    }


    private List<Transaction> loadTransactions(String datasetName) throws IOException {
        InputStream is = openInputStream(datasetName + "/transactions.json");
        Type listType = new TypeToken<ArrayList<Transaction>>() {
        }.getType();
        List<Transaction> transactions = loadObject(is, listType);
        is.close();

        if (transactions == null) {
            throw new IOException("There are no transactions.");
        }
        Collections.sort(transactions, new Comparator<Transaction>() {
            @Override
            public int compare(Transaction transaction, Transaction t1) {
                return transaction.getProductName().compareTo(t1.getProductName());
            }
        });

        return transactions;
    }

    @Override
    public Observable<List<Product>> getProducts() {
        return Observable.create(new Observable.OnSubscribe<List<Product>>() {
            @Override
            public void call(Subscriber<? super List<Product>> subscriber) {
                if (transactionList == null) {
                    try {
                        transactionList = loadTransactions(datasetName);
                    } catch (IOException e) {
                        e.printStackTrace();
                        subscriber.onError(e);
                        return;
                    }
                }

                List<Product> productList = new ArrayList<>();
                for (Transaction t : transactionList) {
                    Product p = new Product(t.getProductName());
                    if (productList.contains(p)) {
                        p = productList.get(productList.indexOf(p));
                        p.addTransaction(t);
                    } else {
                        p.addTransaction(t);
                        productList.add(p);
                    }
                }

                subscriber.onNext(productList);
                subscriber.onCompleted();

            }
        });
    }


    private InputStream openInputStream(String assetPath) throws IOException {
        return context.getAssets().open(assetPath);
    }


    private <T> T loadObject(final InputStream inputStream, final Type type) {
        try {
            if (inputStream != null) {
                final Gson gson = new Gson();
                final BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                return gson.fromJson(reader, type);
            }
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return null;
    }


}
