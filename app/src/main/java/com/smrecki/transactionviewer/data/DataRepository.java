package com.smrecki.transactionviewer.data;

import com.smrecki.transactionviewer.model.ConversionRate;
import com.smrecki.transactionviewer.model.Product;
import com.smrecki.transactionviewer.model.Transaction;

import java.util.List;

import rx.Observable;

/**
 * Created by tomislav on 27/08/16.
 */
public interface DataRepository {

    Observable<List<ConversionRate>> getRates();

    Observable<List<Transaction>> getTransactions();

    Observable<List<Transaction>> getTransactions(String productName);

    Observable<List<Product>> getProducts();


}
