package com.smrecki.transactionviewer.data.djikstra;

/**
 * Created by tomislav on 27/08/16.
 */
public class Edge {
    private final String id;
    private final Vertex source;
    private final Vertex destination;
    private final float weight;

    public Edge(String id, Vertex source, Vertex destination, float weight) {
        this.id = id;
        this.source = source;
        this.destination = destination;
        this.weight = weight;
    }

    public String getId() {
        return id;
    }

    public Vertex getDestination() {
        return destination;
    }

    public Vertex getSource() {
        return source;
    }

    public float getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return source + " " + destination;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Edge)) return false;

        Edge edge = (Edge) o;

        return id != null ? id.equals(edge.id) : edge.id == null;

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
