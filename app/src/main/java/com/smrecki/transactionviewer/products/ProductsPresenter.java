package com.smrecki.transactionviewer.products;

import com.smrecki.transactionviewer.model.Product;
import com.smrecki.transactionviewer.presentation.presenters.BasePresenter;
import com.smrecki.transactionviewer.presentation.ui.BaseView;

import java.util.List;

/**
 * Created by tomislav on 27/08/16.
 */
public interface ProductsPresenter extends BasePresenter {


    interface View extends BaseView {
        void showProducts(List<Product> productList);
    }


}
