package com.smrecki.transactionviewer.products;

import com.smrecki.transactionviewer.data.DataRepository;
import com.smrecki.transactionviewer.model.Product;
import com.smrecki.transactionviewer.presentation.ui.BaseView;

import java.lang.ref.WeakReference;
import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by tomislav on 27/08/16.
 */
public class ProductsPresenterImpl implements ProductsPresenter {

    private WeakReference<View> view;
    private DataRepository dataRepository;

    private List<Product> productList;

    public ProductsPresenterImpl(DataRepository dataRepository) {
        this.dataRepository = dataRepository;
    }

    @Override
    public void bindView(BaseView view) {
        this.view = new WeakReference<>((View) view);

        if (productList == null) {
            loadProducts();
        } else {
            this.view.get().showProducts(productList);
        }

    }

    private void loadProducts() {
        dataRepository.getProducts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<Product>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if (view != null)
                            view.get().showError(e.getMessage());
                    }

                    @Override
                    public void onNext(List<Product> products) {
                        productList = products;
                        if (view != null)
                            view.get().showProducts(products);
                    }
                });
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {
        this.view = null;
    }

    @Override
    public void destroy() {

    }

}
