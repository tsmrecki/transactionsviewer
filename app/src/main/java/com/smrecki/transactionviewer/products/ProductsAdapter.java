package com.smrecki.transactionviewer.products;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smrecki.transactionviewer.R;
import com.smrecki.transactionviewer.model.Product;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by tomislav on 27/08/16.
 */
public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ProductViewHolder> {

    private List<Product> productList = new ArrayList<>();
    private OnProductClickListener onProductClickListener;

    public void setProducts(List<Product> products) {
        this.productList.clear();
        this.productList.addAll(products);
        notifyDataSetChanged();
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ProductViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.product_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        Product product = productList.get(position);
        holder.name.setText(product.getName());
        holder.transCount.setText(holder.itemView.getContext().getString(R.string.num_transactions, product.getTransactionCount()));
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public void setOnProductClickListener(OnProductClickListener onProductClickListener) {
        this.onProductClickListener = onProductClickListener;
    }

    public interface OnProductClickListener {
        void onProductClick(Product product, View v);
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.product_name)
        TextView name;
        @BindView(R.id.product_trans_count)
        TextView transCount;

        public ProductViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onProductClickListener != null) {
                        onProductClickListener.onProductClick(productList.get(getAdapterPosition()), itemView);
                    }
                }
            });
        }

    }
}
