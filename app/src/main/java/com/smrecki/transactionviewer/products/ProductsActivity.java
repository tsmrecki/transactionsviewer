package com.smrecki.transactionviewer.products;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.smrecki.transactionviewer.Dataset;
import com.smrecki.transactionviewer.R;
import com.smrecki.transactionviewer.data.JsonFileRepositoryImpl;
import com.smrecki.transactionviewer.model.Product;
import com.smrecki.transactionviewer.presentation.ui.BaseActivity;
import com.smrecki.transactionviewer.transaction.TransactionsActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ProductsActivity extends BaseActivity<ProductsPresenter> implements ProductsPresenter.View, ProductsAdapter.OnProductClickListener {

    @BindView(R.id.products_rv)
    RecyclerView productsRv;
    ProductsAdapter productsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);
        ButterKnife.bind(this);

        productsRv.setLayoutManager(new LinearLayoutManager(this));
        productsRv.setItemAnimator(new DefaultItemAnimator());

        productsAdapter = new ProductsAdapter();
        productsRv.setAdapter(productsAdapter);

        productsAdapter.setOnProductClickListener(this);
    }

    @Override
    public ProductsPresenter initPresenter() {
        return new ProductsPresenterImpl(JsonFileRepositoryImpl.getInstance(this, Dataset.DATASET_NAME));
    }


    @Override
    public void showProducts(List<Product> productList) {
        productsAdapter.setProducts(productList);
    }

    @Override
    public void onProductClick(Product product, View v) {
        startActivity(TransactionsActivity.buildIntent(product.getName(), this));
    }
}
