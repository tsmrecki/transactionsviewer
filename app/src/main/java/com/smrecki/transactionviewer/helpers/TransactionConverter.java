package com.smrecki.transactionviewer.helpers;

import com.smrecki.transactionviewer.data.djikstra.DijkstraAlgorithm;
import com.smrecki.transactionviewer.data.djikstra.Edge;
import com.smrecki.transactionviewer.data.djikstra.Graph;
import com.smrecki.transactionviewer.data.djikstra.Vertex;
import com.smrecki.transactionviewer.model.ConversionRate;
import com.smrecki.transactionviewer.model.Transaction;
import com.smrecki.transactionviewer.transaction.viewmodel.TransactionPres;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by tomislav on 27/08/16.
 */
public class TransactionConverter {

    public static List<TransactionPres> convertTransactions(List<Transaction> transactions, List<ConversionRate> conversionRates, String toCurrencyId) {
        List<TransactionPres> transactionPresList = new ArrayList<TransactionPres>();
        for (Transaction transaction : transactions) {
            float conversion = getFinalConversionRate(transaction.getCurrency(), toCurrencyId, conversionRates);
            transactionPresList.add(new TransactionPres(transaction.getAmount(), transaction.getCurrency(), conversion * transaction.getAmount()));
        }
        return transactionPresList;
    }

    public static float getFinalConversionRate(String fromCurrencyId, String toCurrencyId, List<ConversionRate> conversionRates) {
        float conversion = 1;

        if (!fromCurrencyId.equals(toCurrencyId)) {
            List<Vertex> currencies = new ArrayList<>();
            List<Edge> rates = new ArrayList<>();

            for (ConversionRate cr : conversionRates) {
                Vertex fromCurrency = new Vertex(cr.getFromCurrency(), cr.getFromCurrency());
                Vertex toCurrency = new Vertex(cr.getToCurrency(), cr.getToCurrency());
                if (!currencies.contains(fromCurrency)) currencies.add(fromCurrency);
                if (!currencies.contains(toCurrency)) currencies.add(toCurrency);

                Edge conversionRate = new Edge(fromCurrency.getName() + "_" + toCurrency.getName(), fromCurrency, toCurrency, cr.getRate());
                rates.add(conversionRate);
            }

            Graph graph = new Graph(currencies, rates);
            DijkstraAlgorithm da = new DijkstraAlgorithm(graph);
            da.execute(new Vertex(fromCurrencyId, fromCurrencyId));
            LinkedList<Vertex> vertices = da.getPath(new Vertex(toCurrencyId, toCurrencyId));

            for (int i = 0; i < vertices.size() - 1; i++) {
                float conversionRate = getConversionRate(vertices.get(i).getName(), vertices.get(i + 1).getName(), conversionRates);
                conversion *= conversionRate;
            }
        }

        return conversion;
    }

    public static float getConversionRate(String fromCurrency, String toCurrency, List<ConversionRate> conversionRates) {
        for (ConversionRate cr : conversionRates) {
            if (cr.getFromCurrency().equals(fromCurrency) && cr.getToCurrency().equals(toCurrency))
                return cr.getRate();
        }
        return 0;
    }


}
