package com.smrecki.transactionviewer.helpers;

import android.app.Activity;
import android.app.Dialog;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.smrecki.transactionviewer.R;


/**
 * Created by tomislav on 27/05/16.
 */
public class DialogHelper {


    public static Dialog buildInfoDialog(Activity activity, @Nullable String title, String description, final View.OnClickListener onOKClickListener) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_info_layout);

        TextView titleV = (TextView) dialog.findViewById(R.id.dialog_title);
        TextView descriptionV = (TextView) dialog.findViewById(R.id.dialog_description);

        if (title != null)
            titleV.setText(title);
        else
            titleV.setVisibility(View.GONE);

        descriptionV.setText(description);

        Button ok = (Button) dialog.findViewById(R.id.dialog_ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                onOKClickListener.onClick(v);
            }
        });

        return dialog;
    }

}
