package com.smrecki.transactionviewer.presentation.ui;

/**
 * Created by tomislav on 25/02/16.
 */
public interface BaseView {

    void showProgressCircle(boolean show);

    void showError(String errorMessage);

    void showToastInfo(String info);

}
