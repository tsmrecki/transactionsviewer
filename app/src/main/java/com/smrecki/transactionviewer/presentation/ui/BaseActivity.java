package com.smrecki.transactionviewer.presentation.ui;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.smrecki.transactionviewer.R;
import com.smrecki.transactionviewer.helpers.DialogHelper;
import com.smrecki.transactionviewer.presentation.presenters.BasePresenter;
import com.smrecki.transactionviewer.presentation.presenters.PresenterManager;

import butterknife.ButterKnife;

/**
 * Created by tomislav on 25/02/16.
 */
public abstract class BaseActivity<TPresenter extends BasePresenter> extends AppCompatActivity implements BaseView {

    protected InputMethodManager inputMethodManager;
    protected NotificationManager notificationManager;
    private TPresenter presenter;
    /**
     * Keeps track if the activity is temporarily destroyed by system (orientation change etc.)
     */
    private boolean isDestroyedBySystem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        if (savedInstanceState == null) {
            presenter = initPresenter();
            System.out.println("not restoring presenter");
        } else {
            presenter = PresenterManager.getInstance().restorePresenter(savedInstanceState);
            if (presenter == null)
                presenter = initPresenter();
            System.out.println("restoring presenter");
        }

    }


    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);

        Toolbar toolbar = (Toolbar) findViewById(R.id.simple_toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
        ButterKnife.bind(this);
    }

    public void setContentViewWithoutInject(@LayoutRes int layoutResId) {
        super.setContentView(layoutResId);

        Toolbar toolbar = (Toolbar) findViewById(R.id.simple_toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }

    }

    public abstract TPresenter initPresenter();

    public TPresenter presenter() {
        return presenter;
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.bindView(this);
        presenter.resume();
        isDestroyedBySystem = false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.pause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // If the activity is being permanently destroyed, destroy the presenter as well
        if (!isDestroyedBySystem) {
            presenter.destroy();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        PresenterManager.getInstance().savePresenter(presenter, outState);
        /*
         * If onSaveInstanceState is called, it means that the activity will be coming back.
         * Set the flag isBeingDestroyedBySystem to true.
         */
        isDestroyedBySystem = true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showError(String errorMessage) {
        DialogHelper.buildInfoDialog(this, null, errorMessage, new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        }).show();

        //Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showToastInfo(String info) {
        Toast.makeText(this, info, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void showProgressCircle(boolean show) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.simple_toolbar);
        if (toolbar != null) {
            ProgressBar progressBar = (ProgressBar) toolbar.findViewById(R.id.progressBar);
            if (progressBar != null) {
                if (show) progressBar.setVisibility(View.VISIBLE);
                else progressBar.setVisibility(View.GONE);
            }
        }
    }
}
