package com.smrecki.transactionviewer.presentation.presenters;


import com.smrecki.transactionviewer.presentation.ui.BaseView;

/**
 * Created by tomislav on 25/02/16.
 */
public interface BasePresenter {


    /**
     * Called when the {@link BaseView view} should be binded to the presenter
     *
     * @param view View that should be binded to the presenter
     */
    void bindView(BaseView view);

    /**
     * Called when the presenter is being resumed from background
     */
    void resume();

    /**
     * Called when the presenter is being put in the background
     */
    void pause();


    /**
     * Called when the presenter should be permanently destroyed
     */
    void destroy();

}
