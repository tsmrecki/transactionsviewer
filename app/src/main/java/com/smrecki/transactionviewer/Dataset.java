package com.smrecki.transactionviewer;

/**
 * Created by tomislav on 27/08/16.
 */
public class Dataset {

    /**
     * Name of the folder for the selected dataset from Assets folder.
     * Can be moved to environment for testing purposes.
     */
    public static final String DATASET_NAME = "firstdata";
}
