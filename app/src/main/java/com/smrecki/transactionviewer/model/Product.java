package com.smrecki.transactionviewer.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tomislav on 27/08/16.
 */
public class Product {

    private String name;
    private List<Transaction> transactionList = new ArrayList<>();

    public Product(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void addTransaction(Transaction transaction) {
        this.transactionList.add(transaction);
    }

    public int getTransactionCount() {
        return transactionList.size();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;

        Product product = (Product) o;

        return name != null ? name.equals(product.name) : product.name == null;

    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }
}
