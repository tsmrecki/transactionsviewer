package com.smrecki.transactionviewer.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tomislav on 27/08/16.
 */
public class Transaction {

    @SerializedName("sku")
    private String productName;

    @SerializedName("amount")
    private float amount;

    @SerializedName("currency")
    private String currency;

    public Transaction(String productName, float amount, String currency) {
        this.productName = productName;
        this.amount = amount;
        this.currency = currency;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

}
