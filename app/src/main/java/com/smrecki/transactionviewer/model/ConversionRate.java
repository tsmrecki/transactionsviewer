package com.smrecki.transactionviewer.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tomislav on 27/08/16.
 */
public class ConversionRate {

    @SerializedName("from")
    private String fromCurrency;

    @SerializedName("to")
    private String toCurrency;

    @SerializedName("rate")
    private float rate;

    public ConversionRate(String fromCurrency, String toCurrency, float rate) {
        this.fromCurrency = fromCurrency;
        this.toCurrency = toCurrency;
        this.rate = rate;
    }

    public String getFromCurrency() {
        return fromCurrency;
    }

    public void setFromCurrency(String fromCurrency) {
        this.fromCurrency = fromCurrency;
    }

    public String getToCurrency() {
        return toCurrency;
    }

    public void setToCurrency(String toCurrency) {
        this.toCurrency = toCurrency;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

}
